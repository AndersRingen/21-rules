import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class main {
    public static void main(String[] args) {
        System.out.println("Enter textfile name:");
        Scanner Scanner = new Scanner (System.in);// Initialize a scanner to retrieve input
        String fileName = Scanner.nextLine();// Retrieves the fileName-input from the next line in the command line interface

        System.out.println( // Printing out the result of the Blackjack-game
                playBlackjack( // Calling the method that plays the game
                        getDeck(fileName))); // playBlackjack takes the deck as input and needs
    }                                        // to either read the .txt-file or create a shuffled deck
                                             // using the getDeck()-method with either the fileName or an empty string


    private static HashMap<String, Integer> createDeckMap() {
        HashMap<String, Integer> deck = new HashMap<>();// Initialize a hashmap to store card values
        String[] suits = {"C", "D", "H", "S"}; // Make an array of the four suits to make the storing easier
        for (int i = 0; i < suits.length; i++) {
            for (int j = 2; j < 11; j++) { // Create a card from value 2-10 for every suit and put it in the hashmap
                deck.put(suits[i]+j, j);
            }
            deck.put(suits[i]+"J", 10);
            deck.put(suits[i]+"Q", 10);// This is where we manually store the card with pictures
            deck.put(suits[i]+"K", 10);
            deck.put(suits[i]+"A", 11);
        }
        return deck; // return the hashmap of every card as key and its integer value as value
    }
    private static ArrayList<String> shuffleDeck() { // This is for making a unique shuffled deck
        HashMap<String, Integer> deck = createDeckMap(); // get the deckMap to get the entire deck
        ArrayList<String> shuffledDeck = new ArrayList<>();
        deck.forEach((k, v) -> shuffledDeck.add(k));// Put all the keys of the deckMap into an array
        Collections.shuffle(shuffledDeck);// shuffle the array making the content into random order
        return shuffledDeck;
    }

    public static ArrayList<String> getDeck(String fileName){
        if (fileName.equals("")){ // If the input from the command line is empty, we return a new shuffled deck
            return shuffleDeck();
        }
        try {
            ArrayList<String> deck = new ArrayList<>(); // We make a new array to write the content of the file onto
            File myObj = new File("Blackjack Game\\src\\deckFiles\\"+fileName);// Make a new file with the path
            Scanner myReader = new Scanner(myObj); // Make a reader to read the file
            while (myReader.hasNext()) { // Reads every word of the file as long as there is a next word
                String str = myReader.next();// Store the word in a string variable
                str = str.replace(",",""); // Strip the word of the comma
                deck.add(str); // Add the card onto the deck arraylist
            }
            myReader.close();
            return deck;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return null;
        }
    }


    // for creating the textbook output
    private static StringBuilder result(String winner, List<String> samCards, List<String> dCards){
        StringBuilder str = new StringBuilder(); // Creating the desired output
        str.append("\n\n---------------------------\n") // Dividing this result output with the other
                .append(winner) // Announcing the winner
                .append("\nsam: ");
        for (int i = 0; i < samCards.size(); i++) { // Looping through every card drawn by Sam
            str.append(samCards.get(i)).append(", "); // and appends it to the StringBuilder
        }
        str.deleteCharAt(str.length()-2); // Deleting the comma on the last card
        str.append("\ndealer: ");
        for (int i = 0; i < dCards.size(); i++) { // Looping through every card drawn by the Dealer
            str.append(dCards.get(i)).append(", "); // and appends it to the StringBuilder
        }
        str.deleteCharAt(str.length()-2); // Deleting the comma on the last card
        return str;
    }




    public static String playBlackjack(ArrayList<String> deck) {
        HashMap<String, Integer> deckMap = createDeckMap(); // Getting the deckMap to retrieve a value of a card
        int samSum = 0; // Initialize the total score of Sam and the dealer
        int dSum = 0;
        List<String> samCards = new ArrayList<>(); // Initialize the list of the cards
        List<String> dCards = new ArrayList<>(); // that will be drawn by the players

        ////// THE INITIAL DRAWING OF CARDS //////
        System.out.println("\nLet the game of Blackjack begin:\n");
        for (int j = 0; j < 4; j++) { // A loop with 4 rounds to draw the first 4 cards
            String card = deck.get(j); // Get the 1-4. numbers of the deck from the function input
            if (j % 2 == 0){ // To see if it is Sam's or the dealers turn
                samSum += deckMap.get(card);
                samCards.add(card); // Add the card to the card list and the value to the total score
                System.out.println("Sam draws: "+card);
            }else {
                dSum += deckMap.get(card);
                dCards.add(card);// Add the card to the card list and the value to the total score
                System.out.println("Dealer draws: "+card);
            }
        }
        if (samSum == 21){ // Sam wins with Blackjack regardless of the dealers hand
            return "\nBLACKJACK! Sam wins."
                    +result("sam", samCards, dCards);
        } else if (dSum == 21) { // Dealer wins when he has 21 total score
            return "\nBLACKJACK! Dealer wins."
                    +result("dealer", samCards, dCards);
        }else if (dSum == 22 && samSum == 22) { // Dealer wins when both draw 22
            return "\nDealer wins! Both players drew a score of 22 in phase 1."
                    +result("dealer", samCards, dCards);
        }
        System.out.println("\nThe players score after the first phase:");
        System.out.println("Sam: "+samSum);
        System.out.println("Dealer: "+dSum+"\n");

        ////// NO INITIAL WINS AND THE GAME CONTINUE //////
        int i = 4; // --> The index to draw cards from the deck after the 4 first cards
        while(samSum < 17){// Sam starts drawing cards until his score is 17 or higher
            String card = deck.get(i++);
            samSum += deckMap.get(card);
            samCards.add(card);
            System.out.println("Sam draws: "+card);
        }// After this we check if he is in between the legal limit of 17-21
        if (samSum > 21){ // Sam loses when exceeding 21
            return "\nDealer wins! Sam's total score exceeded 21 with the score of "+samSum+". Good game!"
                    +result("dealer", samCards, dCards);
        }else { // The dealer draws until his score is higher than Sam's score
            System.out.println("Sam is inside the legal limit with the score of "+samSum+".\nThe dealer's turn to draw:\n");
            while(dSum <= samSum){
                String card = deck.get(i++);
                dSum += deckMap.get(card);
                dCards.add(card);
                System.out.println("Dealer draws: "+card);
            }
        }
        if (dSum > 21){
            return "\nSam wins! The dealer's total score exceeded 21 with the score of "+dSum+". Good game!"
                    +result("sam", samCards, dCards);
        }else {
            return "\nThe dealers wins! His score was "+dSum+", while Sam's score was only "+samSum+". Good game!"
                    +result("dealer", samCards, dCards);
        }
    }
}

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class mainTest {
    @Test
    public void playBlackjack_shouldReturnSamWinByBlackJack() {
        // Arrange
        String[] stringArray = new String[] { "CK", "D10", "SA", "H3" };
        ArrayList<String> deck = new ArrayList<>(Arrays.asList(stringArray));
        String expected = "\n" +
                "BLACKJACK! Sam wins.\n" +
                "\n" +
                "---------------------------\n" +
                "sam\n" +
                "sam: CK, SA \n" +
                "dealer: D10, H3 ";
        // Act
        String actual = main.playBlackjack(deck);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void playBlackjack_shouldReturnDealerWinByBlackJack() {
        // Arrange
        String[] stringArray = new String[] { "D10", "CJ", "S3", "HA" };
        ArrayList<String> deck = new ArrayList<>(Arrays.asList(stringArray));
        String expected = "\n" +
                "BLACKJACK! Dealer wins.\n" +
                "\n" +
                "---------------------------\n" +
                "dealer\n" +
                "sam: D10, S3 \n" +
                "dealer: CJ, HA ";
        // Act
        String actual = main.playBlackjack(deck);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void playBlackjack_shouldReturnSamWinByDoubleBlackJack() {
        // Arrange
        String[] stringArray = new String[] { "D10", "CJ", "SA", "HA" };
        ArrayList<String> deck = new ArrayList<>(Arrays.asList(stringArray));
        String expected = "\n" +
                "BLACKJACK! Sam wins.\n" +
                "\n" +
                "---------------------------\n" +
                "sam\n" +
                "sam: D10, SA \n" +
                "dealer: CJ, HA ";
        // Act
        String actual = main.playBlackjack(deck);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void playBlackjack_shouldReturnDealerWinByDouble22() {
        // Arrange
        String[] stringArray = new String[] { "DA", "CA", "SA", "HA" };
        ArrayList<String> deck = new ArrayList<>(Arrays.asList(stringArray));
        String expected = "\n" +
                "Dealer wins! Both players drew a score of 22 in phase 1.\n" +
                "\n" +
                "---------------------------\n" +
                "dealer\n" +
                "sam: DA, SA \n" +
                "dealer: CA, HA ";
        // Act
        String actual = main.playBlackjack(deck);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void playBlackjack_shouldReturnSamWinByDealerExceeding21() {
        // Arrange
        String[] stringArray = new String[] { "DA", "CA", "S9", "H5", "CJ" };
        ArrayList<String> deck = new ArrayList<>(Arrays.asList(stringArray));
        String expected = "\n" +
                "Sam wins! The dealer's total score exceeded 21 with the score of 26. Good game!\n" +
                "\n" +
                "---------------------------\n" +
                "sam\n" +
                "sam: DA, S9 \n" +
                "dealer: CA, H5, CJ ";
        // Act
        String actual = main.playBlackjack(deck);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void playBlackjack_shouldReturnDealerWinBySamExceeding21() {
        // Arrange
        String[] stringArray = new String[] { "D5", "C7", "S9", "H5", "CJ" };
        ArrayList<String> deck = new ArrayList<>(Arrays.asList(stringArray));
        String expected = "\n" +
                "Dealer wins! Sam's total score exceeded 21 with the score of 24. Good game!\n" +
                "\n" +
                "---------------------------\n" +
                "dealer\n" +
                "sam: D5, S9, CJ \n" +
                "dealer: C7, H5 ";
        // Act
        String actual = main.playBlackjack(deck);
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void getDeck_shouldReturnRandomShuffledDeck() {
        // Arrange
        String input = "";
        // Act
        ArrayList<String> deck1 = main.getDeck(input);
        ArrayList<String> deck2 = main.getDeck(input);
        //Assert
        assertNotEquals(deck1, deck2);
    }

}